﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Bank.Accounts;
using Bank.Interests;
using Bank.Transactions;
using Deposit = Bank.Accounts.Deposit;

namespace Bank
{
    public class Bank
    {
        private readonly ConcurrentDictionary<Client, ConcurrentBag<AccountBase>> _clients =
            new ConcurrentDictionary<Client, ConcurrentBag<AccountBase>>();

        public IReadOnlyDictionary<Client, List<AccountBase>> Clients
            => new ReadOnlyDictionary<Client, List<AccountBase>>(_clients.ToDictionary(
                pair => pair.Key, pair => pair.Value.ToList()));

        public string Name { get; }
        public History History { get; } = new History();

        public Bank(string name)
        {
            Name = name;
            BankManager.Banks.Add(this);
        }

        public Guid CreateNewStandardAccount(Guid clientId, InterestBase interest = null)
        {
            var account = Standard.CreateStandardAccount(clientId, interest ?? ConstantInterest.GetInterest(1.0));
            AddAccount(account);
            return account.AccountNumber;
        }

        public Guid CreateNewCredit(Guid mainAccount, decimal creditAmount, InterestBase interest = null)
        {
            var account = new BankManager().GetAccount<Standard>(mainAccount);
            interest = interest ?? CalculateInterest(account.OwnerId, creditAmount);
            var credit = Credit.CreateCredit(account, interest, creditAmount);
            AddAccount(credit);
            return credit.AccountNumber;
        }

        public Guid CreateNewDeposit(Guid mainAccount, TimeSpan dueTime, InterestBase interest = null)
        {
            var account = new BankManager().GetAccount<Standard>(mainAccount);
            interest = interest ?? CalculateInterest(account.OwnerId, dueTime);
            var deposit = Deposit.CreateDeposit(account, interest, dueTime);
            AddAccount(deposit);
            return deposit.AccountNumber;
        }

        public void RegisterNewClient(Client client)
        {
            _clients.TryAdd(client, new ConcurrentBag<AccountBase>());
        }

        public List<AccountBase> GetClientAccounts(Client client)
        {
            return _clients[client].ToList();
        }

        public void ReplaceAccount(Standard standard, Standard newStandard)
        {
            var client = GetClient(standard.OwnerId);
            AccountBase removed;

            if (client == null || !_clients[client].TryTake(out removed))
                throw new Exception();

            _clients[client].Add(newStandard);
        }

        private void AddAccount(AccountBase account)
        {
            var client = GetClient(account.OwnerId);
            if (client == null)
                throw new Exception("Invalid client");

            _clients[client].Add(account);
            History.Register(account);
        }

        private static InterestBase CalculateInterest(Guid client, decimal creditAmount)
        {
            int seed;

            unchecked
            {
                seed = client.GetHashCode();
                seed += (int) creditAmount;
            }

            return ConstantInterest.GetInterest(new Random(seed).Next(20, 100)/1000.0 + 1.0);
        }

        private static InterestBase CalculateInterest(Guid client, TimeSpan dueTime)
        {
            int seed;

            unchecked
            {
                seed = client.GetHashCode();
                seed += (int) dueTime.TotalDays;
            }

            return ConstantInterest.GetInterest(new Random(seed).Next(20, 100)/1000.0 + 1.0);
        }

        private Client GetClient(Guid guid)
        {
            return Clients.Keys.SingleOrDefault(client => client.ClientId == guid);
        }
    }
}