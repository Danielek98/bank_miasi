﻿using System;
using Bank.Accounts;
using Bank.Transactions;
using Deposit = Bank.Transactions.Deposit;

namespace Bank
{
    public class TransferMediator
    {
        public BankManager BankManager { get; set; } = new BankManager();

        public void TransferMoney(Guid from, Guid to, decimal amount)
        {
            var fromAccount = BankManager.GetAccount<Standard>(from);
            var withdraw = new Withdraw(fromAccount, amount)
            {
                BankManager = BankManager
            };
            withdraw.Execute();

            try
            {
                var toAccount = BankManager.GetAccount<Standard>(to);
                var deposit = new Deposit(toAccount, amount)
                {
                    BankManager = BankManager
                };
                deposit.Execute();
            }
            catch (Exception)
            {
                var deposit = new Deposit(fromAccount, amount)
                {
                    BankManager = BankManager
                };
                deposit.Execute();
            }
        }
    }
}