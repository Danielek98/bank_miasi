﻿using System.Collections.Generic;
using System.Linq;
using Bank.Accounts;
using Bank.Transactions;

namespace Bank
{
    public static class Report
    {
        public static string GenerateHistoryReport(this IEnumerable<Command> history)
        {
            return string.Join("\n", history);
        }

        public static IDictionary<Client, decimal> CalculateTotalAssetsPerClient(
            this Dictionary<Client, IEnumerable<AccountBase>> accounts)
        {
            return new Dictionary<Client, decimal>(accounts.ToDictionary(pair => pair.Key,
                pair => pair.Value.Sum(account => account.Balance)));
        }

        public static IDictionary<Client, IList<AccountBase>> GetProductsPerClient(
            this Dictionary<Client, IEnumerable<AccountBase>> accounts, decimal minBalance = decimal.MinValue,
            decimal maxBalance = decimal.MaxValue)
        {
            return new Dictionary<Client, IList<AccountBase>>(accounts.ToDictionary(pair => pair.Key,
                pair =>
                    (IList<AccountBase>) pair.Value.Where(
                        account => account.Balance >= minBalance && account.Balance <= maxBalance).ToList()));
        }

        public static decimal CalculateTotalAssets(
            this Dictionary<Client, IEnumerable<AccountBase>> accounts)
        {
            return accounts.CalculateTotalAssetsPerClient().Sum(pair => pair.Value);
        }

        public static IList<AccountBase> GetProducts(
            this Dictionary<Client, IEnumerable<AccountBase>> accounts, decimal minBalance = decimal.MinValue,
            decimal maxBalance = decimal.MaxValue)
        {
            return accounts.GetProductsPerClient(minBalance, maxBalance).SelectMany(pair => pair.Value).ToList();
        }
    }
}