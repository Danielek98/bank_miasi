﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using Bank.Accounts;

namespace Bank
{
    public class BankManager
    {
        internal static readonly ConcurrentBag<Bank> Banks = new ConcurrentBag<Bank>();

        public virtual TAccount GetAccount<TAccount>(Guid account)
            where TAccount : AccountBase
        {
            return Banks.SelectMany(bank => bank.Clients)
                .SelectMany(pair => pair.Value)
                .OfType<TAccount>()
                .SingleOrDefault(x => x.AccountNumber == account);
        }

        public virtual List<AccountBase> GetProducts(Client client)
        {
            return Banks.SelectMany(bank => bank.Clients)
                .Where(pair => pair.Key == client)
                .SelectMany(pair => pair.Value)
                .ToList();
        }

        public virtual Bank GetBank(Guid accountNumber)
        {
            return Banks.SingleOrDefault(bank =>
                bank.Clients
                    .SelectMany(client => client.Value)
                    .Any(account => account.AccountNumber == accountNumber));
        }

        public virtual IEnumerable<AccountBase> GetAllAccounts()
        {
            return Banks.SelectMany(bank => bank.Clients)
                .SelectMany(pair => pair.Value);
        }
    }
}