﻿using System.Collections.Generic;
using Bank.Accounts;

namespace Bank.Reports
{
    public class ClientAccountsTypeStats : IReportVisitor
    {
        public Dictionary<string, int> Stats { get; } = new Dictionary<string, int>();

        public void Generate(Credit credit)
        {
            CountStats(credit);
        }

        public void Generate(Deposit deposit)
        {
            CountStats(deposit);
        }

        public void Generate(Standard standard)
        {
            CountStats(standard);
        }

        private void CountStats<TAccount>(TAccount account) where TAccount : AccountBase
        {
            var accountType = typeof (TAccount).Name;
            if (!Stats.ContainsKey(accountType))
                Stats.Add(accountType, 0);
            Stats[accountType]++;
        }
    }
}