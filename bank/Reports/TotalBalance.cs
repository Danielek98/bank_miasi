﻿using Bank.Accounts;

namespace Bank.Reports
{
    public class TotalBalance : IReportVisitor
    {
        public decimal Total { get; private set; }

        public void Generate(Credit credit)
        {
            CountStats(credit);
        }

        public void Generate(Deposit deposit)
        {
            CountStats(deposit);
        }

        public void Generate(Standard standard)
        {
            CountStats(standard);
        }

        private void CountStats(AccountBase account)
        {
            Total += account.Balance;
        }
    }
}