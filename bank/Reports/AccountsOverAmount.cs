﻿using Bank.Accounts;

namespace Bank.Reports
{
    public class AccountsOverAmount : IReportVisitor
    {
        private readonly decimal _limit;

        public int NumberOfAccounts { get; private set; }

        public AccountsOverAmount(decimal limit)
        {
            _limit = limit;
        }

        public void Generate(Credit credit)
        {
            CountStats(credit);
        }

        public void Generate(Deposit deposit)
        {
            CountStats(deposit);
        }

        public void Generate(Standard standard)
        {
            CountStats(standard);
        }

        private void CountStats(AccountBase account)
        {
            if (account.Balance > _limit)
                NumberOfAccounts++;
        }
    }
}