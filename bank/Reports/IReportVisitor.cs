﻿using Bank.Accounts;

namespace Bank.Reports
{
    public interface IReportVisitor
    {
        void Generate(Credit credit);
        void Generate(Deposit deposit);
        void Generate(Standard standard);
    }
}
