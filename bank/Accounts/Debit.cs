﻿using System;

namespace Bank.Accounts
{
    public class Debit : Standard
    {
        private readonly Standard _standard;
        private decimal _balance;
        private decimal _debitLimit;

        public override Guid OwnerId => _standard.OwnerId;

        public override DateTime CreationDate
        {
            get { return _standard.CreationDate; }
            protected internal set { _standard.CreationDate = value; }
        }

        public override Guid AccountNumber
        {
            get { return _standard.AccountNumber; }
            protected internal set { _standard.AccountNumber = value; }
        }

        public override decimal Balance
        {
            get { return _balance + _standard.Balance; }
            set
            {
                _balance = Math.Min(value - _standard.Balance, 0);
                _standard.Balance = value - _balance;
            }
        }

        public override decimal DebitLimit => _standard.DebitLimit + _debitLimit;

        private Debit(Standard standard, decimal debitLimit) : base(standard.OwnerId, standard.Interest)
        {
            _standard = standard;
            _debitLimit = debitLimit;
        }

        public override Standard ChangeDebitLimit(decimal newDebitLimit)
        {
            if (newDebitLimit < 0 || newDebitLimit > DebitLimit)
                return base.ChangeDebitLimit(newDebitLimit);

            if (_debitLimit <= newDebitLimit - _standard.DebitLimit)
                return _standard.ChangeDebitLimit(newDebitLimit);

            _debitLimit = newDebitLimit - _standard.DebitLimit;
            return this;
        }

        public static Standard CreateDebit(Standard standard, decimal debitLimit)
        {
            if (debitLimit <= 0)
                throw new ArgumentException();

            if (standard == null)
                throw new ArgumentNullException(nameof(standard));

            return new Debit(standard, debitLimit)
            {
                AccountNumber = standard.AccountNumber,
                CreationDate = standard.CreationDate
            };
        }
    }
}