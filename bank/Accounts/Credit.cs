﻿using System;
using Bank.Interests;
using Bank.Reports;

namespace Bank.Accounts
{
    public class Credit : AccountBase, IAccountElement
    {
        public decimal CreditLimit { get; }
        public Guid MainAccount { get; }
        public bool IsTaken { get; internal set; }
        public bool IsPaid => IsTaken && Balance == 0;

        private Credit(AccountBase mainAccount, InterestBase interest, decimal creditLimit)
            : base(mainAccount.OwnerId, interest)
        {
            MainAccount = mainAccount.AccountNumber;
            CreditLimit = creditLimit;
        }

        public static Credit CreateCredit(Standard mainAccount, InterestBase interest, decimal creditLimit)
        {
            if (mainAccount == null)
                throw new ArgumentNullException(nameof(mainAccount));
            if (interest == null)
                throw new ArgumentNullException(nameof(interest));
            if (creditLimit <= 0)
                throw new ArgumentOutOfRangeException(nameof(creditLimit),
                    "Cannot create a credit account with nonpositive limit");

            return new Credit(mainAccount, interest, creditLimit);
        }

        public void AcceptReportVisitor(IReportVisitor reportVisitor)
        {
            reportVisitor.Generate(this);
        }
    }
}