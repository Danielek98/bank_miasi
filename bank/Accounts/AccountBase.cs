﻿using System;
using Bank.Interests;
using Bank.Reports;
using Bank.Transactions;

namespace Bank.Accounts
{
    public abstract class AccountBase : ICloneable
    {
        private InterestBase _interest;
        public virtual Guid OwnerId { get; }
        public virtual DateTime CreationDate { get; protected internal set; } = DateTime.Now;
        public virtual Guid AccountNumber { get; protected internal set; } = Guid.NewGuid();
        public virtual decimal Balance { get; set; }

        public virtual InterestBase Interest
        {
            get { return _interest; }
            internal set { _interest = value; }
        }

        

        protected AccountBase(Guid ownerId, InterestBase interest)
        {
            OwnerId = ownerId;
            _interest = interest;
        }

        public object Clone()
        {
            return MemberwiseClone();
        }

        public event Action<Command> CommandHistory = delegate { };

        public void Publish(Command command)
        {
            CommandHistory(command);
        }
    }
}