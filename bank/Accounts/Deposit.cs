﻿using System;
using Bank.Interests;
using Bank.Reports;

namespace Bank.Accounts
{
    public class Deposit : AccountBase, IAccountElement
    {
        public Guid MainAccount { get; }
        public bool Deposited { get; internal set; }
        public DateTime DueDateTime { get; internal set; }
        public TimeSpan Duration { get; }
        public bool IsEnded => Deposited && Balance == 0;

        private Deposit(AccountBase mainAccount, InterestBase interest, TimeSpan duration)
            : base(mainAccount.OwnerId, interest)
        {
            MainAccount = mainAccount.AccountNumber;
            Duration = duration;
        }

        public static Deposit CreateDeposit(Standard main, InterestBase interest, TimeSpan duration)
        {
            if (main == null)
                throw new ArgumentNullException(nameof(main));
            if (interest == null)
                throw new ArgumentNullException(nameof(interest));
            if (duration.Duration() <= TimeSpan.Zero)
                throw new ArgumentOutOfRangeException(nameof(duration),
                    "Due date cannot be set to a moment in the past");

            return new Deposit(main, interest, duration);
        }

        public void AcceptReportVisitor(IReportVisitor reportVisitor)
        {
            reportVisitor.Generate(this);
        }
    }
}