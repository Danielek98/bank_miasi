﻿using Bank.Reports;

namespace Bank.Accounts
{
    public interface IAccountElement
    {
        void AcceptReportVisitor(IReportVisitor reportVisitor);
    }
}
