﻿using System;
using Bank.Interests;
using Bank.Reports;

namespace Bank.Accounts
{
    public class Standard : AccountBase, IAccountElement
    {
        public virtual decimal DebitLimit { get; } = 0;

        protected Standard(Guid ownerId, InterestBase interest) : base(ownerId, interest)
        {
        }

        public static Standard CreateStandardAccount(Guid ownerId, InterestBase interest)
        {
            return new Standard(ownerId, interest);
        }

        public virtual Standard ChangeDebitLimit(decimal newDebitLimit)
        {
            if (newDebitLimit < 0 || newDebitLimit == DebitLimit)
                throw new InvalidOperationException();

            return Debit.CreateDebit(this, newDebitLimit - DebitLimit);
        }

        public virtual void AcceptReportVisitor(IReportVisitor reportVisitor)
        {
            reportVisitor.Generate(this);
        }
    }
}