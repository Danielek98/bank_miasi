﻿using System;
using Bank.Accounts;

namespace Bank.Interests
{
    public class ConstantInterest : InterestBase
    {
        private readonly double _factor;

        private ConstantInterest(double factor)
        {
            _factor = factor;
        }

        public override decimal CalculateInterest(AccountBase account)
        {
            return Math.Round(account.Balance*(decimal) _factor, 2, MidpointRounding.AwayFromZero);
        }

        public static ConstantInterest GetInterest(double factor)
        {
            if (factor < 1.0)
                throw new ArgumentException("value lower than 1.0", nameof(factor));

            return new ConstantInterest(factor);
        }
    }
}