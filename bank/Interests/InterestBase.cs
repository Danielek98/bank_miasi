﻿using Bank.Accounts;

namespace Bank.Interests
{
    public abstract class InterestBase
    {
        public abstract decimal CalculateInterest(AccountBase account);
    }
}