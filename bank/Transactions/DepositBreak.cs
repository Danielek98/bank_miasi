﻿namespace Bank.Transactions
{
    public class DepositBreak : AccountCommand<Accounts.Deposit>
    {
        public DepositBreak(Accounts.Deposit account) : base(account, account.Balance)
        {
        }

        protected override void CommandLogic()
        {
        }
    }
}