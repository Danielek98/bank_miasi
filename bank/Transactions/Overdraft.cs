﻿using Bank.Accounts;

namespace Bank.Transactions
{
    public class Overdraft : AccountCommand<Standard>
    {
        public Overdraft(Standard account) : base(account, account.Balance)
        {
        }

        protected override void CommandLogic()
        {
        }
    }
}