﻿using System;
using Bank.Accounts;

namespace Bank.Transactions
{
    public class DepositWithdraw : AccountCommand<Accounts.Deposit>
    {
        public DepositWithdraw(Accounts.Deposit account) : base(account, account.Balance)
        {
        }

        protected override void CommandLogic()
        {
            lock (Account)
            {
                if (!Account.Deposited)
                    throw new InvalidOperationException();

                if (DateTime.Now >= Account.DueDateTime)
                    new CalculateInterest<Accounts.Deposit>(Account).Execute();
                else
                    new DepositBreak(Account).Execute();

                new Deposit(BankManager.GetAccount<Standard>(Account.MainAccount), Account.Balance).Execute();
                Account.Balance = 0;
            }
        }
    }
}