﻿using Bank.Accounts;

namespace Bank.Transactions
{
    public abstract class AccountCommand<TAccount> : Command where TAccount : AccountBase
    {
        public TAccount Account { get; }
        public decimal Amount { get; }
        public virtual AccountBase ConnectedAccount => null;
        public TAccount AccountSnapshot { get; private set; }
        public AccountBase ConnectedAccountSnapshot { get; private set; }
        public BankManager BankManager { get; set; } = new BankManager();

        protected AccountCommand(TAccount account, decimal amount)
        {
            Account = account;
            Amount = amount;
        }

        public override void Execute(object parameter = null)
        {
            base.Execute(parameter);
            AccountSnapshot = (TAccount) Account.Clone();
            ConnectedAccountSnapshot = (AccountBase) ConnectedAccount?.Clone();
            Account.Publish(this);
        }

        public override string ToString()
        {
            return $"{GetType().Name}: {Account.AccountNumber}\t{Amount}\t{ConnectedAccount?.AccountNumber}";
        }
    }
}