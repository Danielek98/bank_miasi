﻿using Bank.Accounts;
using Bank.Interests;

namespace Bank.Transactions
{
    public class CalculateInterest<TAccount> : AccountCommand<TAccount> where TAccount : AccountBase
    {
        public InterestBase Interest { get; }

        public CalculateInterest(TAccount account) : base(account, account.Balance)
        {
            Interest = account.Interest;
        }

        protected override void CommandLogic()
        {
            lock (Account)
            {
                Account.Balance = Interest.CalculateInterest(Account);
            }
        }
    }
}