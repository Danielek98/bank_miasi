﻿using System;
using Bank.Accounts;

namespace Bank.Transactions
{
    public class DepositAssumption : AccountCommand<Accounts.Deposit>
    {
        public DepositAssumption(Accounts.Deposit account, decimal amount) : base(account, amount)
        {
        }

        protected override void CommandLogic()
        {
            lock (Account)
            {
                if (Account.Deposited)
                    throw new InvalidOperationException();

                new Withdraw(BankManager.GetAccount<Standard>(Account.MainAccount), Amount).Execute();

                Account.DueDateTime = DateTime.Now.Add(Account.Duration);
                Account.Balance += Amount;
                Account.Deposited = true;
            }
        }
    }
}