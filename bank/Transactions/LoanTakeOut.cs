﻿using System;
using Bank.Accounts;

namespace Bank.Transactions
{
    public class LoanTakeOut : AccountCommand<Credit>
    {
        public LoanTakeOut(Credit account, decimal amount) : base(account, amount)
        {
        }

        protected override void CommandLogic()
        {
            lock (Account)
            {
                if (Account.IsTaken || Amount <= 0 || Amount > Account.CreditLimit)
                    throw new InvalidOperationException();

                new Deposit(BankManager.GetAccount<Standard>(Account.MainAccount), Amount).Execute();

                Account.Balance -= Amount;
                Account.IsTaken = true;
                new CalculateInterest<Credit>(Account).Execute();
            }
        }
    }
}