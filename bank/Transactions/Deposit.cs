﻿using System;
using Bank.Accounts;

namespace Bank.Transactions
{
    public class Deposit : AccountCommand<Standard>
    {
        public Deposit(Standard account, decimal amount) : base(account, amount)
        {
        }

        protected override void CommandLogic()
        {
            lock (Account)
            {
                if (Amount <= 0)
                    throw new InvalidOperationException();
                Account.Balance += Amount;
            }
        }
    }
}