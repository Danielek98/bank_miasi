﻿using System;
using Bank.Accounts;

namespace Bank.Transactions
{
    public class ChangeDebit : AccountCommand<Standard>
    {
        private Standard _connectedAccount;
        public override AccountBase ConnectedAccount => _connectedAccount;

        public ChangeDebit(Standard account, decimal amount) : base(account, amount)
        {
        }

        protected override void CommandLogic()
        {
            lock (Account)
            {
                if (Amount < 0 || Amount < -Account.Balance)
                    throw new InvalidOperationException();

                _connectedAccount = Account.ChangeDebitLimit(Amount);
                BankManager.GetBank(Account.AccountNumber)?.ReplaceAccount(Account, _connectedAccount);
            }
        }
    }
}