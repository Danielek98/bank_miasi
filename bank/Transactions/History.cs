﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using Bank.Accounts;

namespace Bank.Transactions
{
    public class History
    {
        private readonly ConcurrentQueue<Command> _transactions = new ConcurrentQueue<Command>();

        public void Register(AccountBase account)
        {
            account.CommandHistory += _transactions.Enqueue;
        }

        public void Unregister(AccountBase account)
        {
            account.CommandHistory -= _transactions.Enqueue;
        }

        public virtual IList<Command> GetHistory()
        {
            return _transactions.ToList();
        }

        public virtual IList<AccountCommand<TAccount>> GetHistory<TAccount>(TAccount product)
            where TAccount : AccountBase
        {
            return _transactions.OfType<AccountCommand<TAccount>>().Where(transaction =>
                transaction.Account == product ||
                transaction.ConnectedAccount != null &&
                transaction.ConnectedAccount == product)
                .ToList();
        }
    }
}