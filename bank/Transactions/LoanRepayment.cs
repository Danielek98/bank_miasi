﻿using System;
using Bank.Accounts;

namespace Bank.Transactions
{
    public class LoanRepayment : AccountCommand<Credit>
    {
        public LoanRepayment(Credit account) : base(account, account.Balance)
        {
        }

        protected override void CommandLogic()
        {
            lock (Account)
            {
                if (!Account.IsTaken)
                    throw new InvalidOperationException();

                new Withdraw(BankManager.GetAccount<Standard>(Account.MainAccount), -Amount).Execute();
                Account.Balance = 0;
            }
        }
    }
}