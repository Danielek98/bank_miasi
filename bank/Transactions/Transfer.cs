﻿using System;
using Bank.Accounts;

namespace Bank.Transactions
{
    public class Transfer : AccountCommand<Standard>
    {
        private readonly Standard _to;
        public override AccountBase ConnectedAccount => _to;

        public Transfer(Standard @from, decimal amount, Standard to) : base(@from, amount)
        {
            _to = to;
        }

        public override void Execute(object parameter = null)
        {
            base.Execute(parameter);
            _to.Publish(this);
        }

        protected override void CommandLogic()
        {
            lock (Account)
            {
                if (Amount <= 0)
                    throw new InvalidOperationException();

                var transferMediator = new TransferMediator
                {
                    BankManager = BankManager
                };
                transferMediator.TransferMoney(Account.AccountNumber, _to.AccountNumber, Amount);
            }
        }
    }
}