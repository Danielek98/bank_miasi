﻿using System;
using Bank.Accounts;

namespace Bank.Transactions
{
    public class Withdraw : AccountCommand<Standard>
    {
        public Withdraw(Standard account, decimal amount) : base(account, amount)
        {
        }

        protected override void CommandLogic()
        {
            lock (Account)
            {
                if (Amount <= 0 || Amount > Account.Balance + Account.DebitLimit)
                    throw new InvalidOperationException();

                Account.Balance -= Amount;

                if (Account.Balance < 0 && Amount > -Account.Balance)
                    new Overdraft(Account).Execute();
            }
        }
    }
}