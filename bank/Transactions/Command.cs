﻿using System;
using System.Windows.Input;

namespace Bank.Transactions
{
    public abstract class Command : ICommand
    {
        private readonly object _executeLock = new object();
        private bool _canExecute = true;

        public bool CanExecute(object parameter)
        {
            lock (_executeLock)
            {
                return _canExecute;
            }
        }

        public virtual void Execute(object parameter = null)
        {
            lock (_executeLock)
            {
                if (!_canExecute)
                    throw new Exception();

                CommandLogic();
                _canExecute = false;
                CanExecuteChanged(this, EventArgs.Empty);
            }
        }

        public event EventHandler CanExecuteChanged = delegate { };

        protected abstract void CommandLogic();
    }
}