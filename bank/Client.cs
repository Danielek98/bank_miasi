﻿using System;

namespace Bank
{
    public class Client
    {
        public virtual Guid ClientId { get; } = Guid.NewGuid();
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public Client(string firstName, string lastName)
        {
            FirstName = firstName;
            LastName = lastName;
        }
    }
}