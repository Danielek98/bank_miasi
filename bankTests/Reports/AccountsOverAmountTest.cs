﻿using System;
using Bank;
using Bank.Accounts;
using Bank.Interests;
using Bank.Reports;
using Bank.Transactions;
using Moq;
using NUnit.Framework;
using Deposit = Bank.Accounts.Deposit;

namespace bankTests.Reports
{
    [TestFixture]
    internal class AccountsOverAmountTest
    {
        [SetUp]
        public void SetUp()
        {
            _accountsOverAmountReport = new AccountsOverAmount(1000);
            var bankManagerMock = new Mock<BankManager>();
            _account1 = Standard.CreateStandardAccount(Guid.Empty, null);
            _account2 = Standard.CreateStandardAccount(Guid.Empty, null);
            _account3 = Standard.CreateStandardAccount(Guid.Empty, null);
            bankManagerMock.Setup(manager => manager.GetAccount<Standard>(It.IsAny<Guid>())).Returns(_account3);
            var changeDebit = new ChangeDebit(_account3, 10m)
            {
                BankManager = bankManagerMock.Object
            };
            changeDebit.Execute();
            _account3 = (Standard) changeDebit.ConnectedAccount;
            _credit1 = Credit.CreateCredit(_account1, ConstantInterest.GetInterest(1.1), 1000);
            _deposit1 = Deposit.CreateDeposit(_account1, ConstantInterest.GetInterest(1.1), TimeSpan.FromDays(1));
            bankManagerMock.Setup(manager => manager.GetAllAccounts())
                .Returns(() => new AccountBase[] {_account1, _account2, _account3, _credit1, _deposit1});
            _bankManager = bankManagerMock.Object;
        }

        private AccountsOverAmount _accountsOverAmountReport;
        private BankManager _bankManager;
        private Standard _account1;
        private Standard _account2;
        private Standard _account3;

        private static readonly object[] GenerateReportTestCases =
        {
            new object[] {1000m, 1100m, 900m, 1},
            new object[] {1200m, 1100m, 1001m, 3},
            new object[] {900m, 900m, 900m, 0},
            new object[] {1200m, 990m, 1100m, 2},
            new object[] {1100m, 1100m, 1100m, 3}
        };

        private Credit _credit1;
        private Deposit _deposit1;

        [Test, TestCaseSource(nameof(GenerateReportTestCases))]
        public void GenerateReportTest(decimal deposit1, decimal deposit2, decimal deposit3, int result)
        {
            new Bank.Transactions.Deposit(_account1, deposit1).Execute();
            new Bank.Transactions.Deposit(_account2, deposit2).Execute();
            new Bank.Transactions.Deposit(_account3, deposit3).Execute();
            var accounts = _bankManager.GetAllAccounts();
            foreach (var account in accounts)
                (account as IAccountElement)?.AcceptReportVisitor(_accountsOverAmountReport);

            Assert.AreEqual(result, _accountsOverAmountReport.NumberOfAccounts);
        }
    }
}