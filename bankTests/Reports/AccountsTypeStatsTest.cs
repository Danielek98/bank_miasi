﻿using System;
using Bank;
using Bank.Accounts;
using Bank.Interests;
using Bank.Reports;
using Bank.Transactions;
using Moq;
using NUnit.Framework;
using Deposit = Bank.Accounts.Deposit;

namespace bankTests.Reports
{
    [TestFixture]
    internal class AccountsTypeStatsTest
    {
        [SetUp]
        public void SetUp()
        {
            _clientAccountsTypeStatsReport = new ClientAccountsTypeStats();
            var bankManagerMock = new Mock<BankManager>();
            _account1 = Standard.CreateStandardAccount(Guid.Empty, null);
            _account2 = Standard.CreateStandardAccount(Guid.Empty, null);
            _account3 = Standard.CreateStandardAccount(Guid.Empty, null);
            bankManagerMock.Setup(manager => manager.GetAccount<Standard>(It.IsAny<Guid>())).Returns(_account3);
            var changeDebit = new ChangeDebit(_account3, 10m)
            {
                BankManager = bankManagerMock.Object
            };
            changeDebit.Execute();
            _account3 = (Standard) changeDebit.ConnectedAccount;
            _credit1 = Credit.CreateCredit(_account1, ConstantInterest.GetInterest(1.1), 1000);
            _deposit1 = Deposit.CreateDeposit(_account1, ConstantInterest.GetInterest(1.1), TimeSpan.FromDays(1));
            bankManagerMock.Setup(manager => manager.GetAllAccounts())
                .Returns(() => new AccountBase[] {_account1, _account2, _account3, _credit1, _deposit1});
            _bankManager = bankManagerMock.Object;
        }

        private ClientAccountsTypeStats _clientAccountsTypeStatsReport;
        private BankManager _bankManager;
        private Standard _account1;
        private Standard _account2;
        private Standard _account3;
        private Credit _credit1;
        private Deposit _deposit1;

        [Test]
        public void GenerateReportTest()
        {
            var accounts = _bankManager.GetAllAccounts();
            foreach (var account in accounts)
                (account as IAccountElement)?.AcceptReportVisitor(_clientAccountsTypeStatsReport);

            Assert.AreEqual(3, _clientAccountsTypeStatsReport.Stats[typeof (Standard).Name]);
            Assert.AreEqual(1, _clientAccountsTypeStatsReport.Stats[typeof (Credit).Name]);
            Assert.AreEqual(1, _clientAccountsTypeStatsReport.Stats[typeof (Deposit).Name]);
        }
    }
}