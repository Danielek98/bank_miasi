﻿using System;
using System.Threading;
using Bank;
using Bank.Accounts;
using Bank.Interests;
using Bank.Transactions;
using Moq;
using NUnit.Framework;
using Deposit = Bank.Accounts.Deposit;

namespace bankTests.Account
{
    [TestFixture]
    internal class DepositTest
    {
        [SetUp]
        public void SetUp()
        {
            var bankManagerMock = new Mock<BankManager>();
            _mainAccount = Standard.CreateStandardAccount(Guid.Empty, null);
            _interest = ConstantInterest.GetInterest(InterestFactor);
            _accountBeforeDueDate = Deposit.CreateDeposit(_mainAccount, _interest, TimeSpan.FromDays(1));
            _accountAfterDueDate = Deposit.CreateDeposit(_mainAccount, _interest, TimeSpan.FromMilliseconds(10));
            bankManagerMock.Setup(manager => manager.GetAccount<Standard>(It.IsAny<Guid>())).Returns(_mainAccount);
            bankManagerMock.Setup(manager => manager.GetAccount<Deposit>(It.IsAny<Guid>()))
                .Returns<Guid>(
                    guid => guid == _accountBeforeDueDate.AccountNumber ? _accountBeforeDueDate : _accountAfterDueDate);
            _bankManager = bankManagerMock.Object;
        }

        private InterestBase _interest;
        private const double InterestFactor = 1.5;
        private Deposit _accountBeforeDueDate;
        private Deposit _accountAfterDueDate;
        private Standard _mainAccount;
        private BankManager _bankManager;

        private static readonly object[] DepositTestCases =
        {
            new object[] {3m, 3m, true},
            new object[] {5m, -5m, false},
            new object[] {1m, 0m, false},
            new object[] {1m, 2m, false},
            new object[] {5m, 2m, true},
            new object[] {3m, 2m, true}
        };

        private static readonly object[] WithdrawTestCases =
        {
            new object[] {10m, 15m, true},
            new object[] {100m, 150m, true},
            new object[] {0m, 0m, false}
        };

        public static readonly object[] ConstructorTestCases =
        {
            new object[] {true, true, TimeSpan.FromDays(1), true},
            new object[] {false, true, TimeSpan.FromDays(1), false},
            new object[] {true, false, TimeSpan.FromDays(1), false},
            new object[] {true, true, TimeSpan.FromDays(0), false}
        };

        [Test, TestCaseSource(nameof(ConstructorTestCases))]
        public void ConstructorTest(bool account, bool interest, TimeSpan dueTime, bool result)
        {
            try
            {
                Deposit.CreateDeposit(account ? _mainAccount : null,
                    interest ? ConstantInterest.GetInterest(InterestFactor) : null, dueTime);

                if (!result)
                    Assert.Fail();
            }
            catch (ArgumentOutOfRangeException)
            {
                if (result)
                    Assert.Fail();
            }
            catch (ArgumentNullException)
            {
                if (result)
                    Assert.Fail();
            }
        }

        [Test, TestCaseSource(nameof(DepositTestCases))]
        public void DepositBalanceTest(decimal amount, decimal deposit, bool result)
        {
            if (amount > 0)
                new Bank.Transactions.Deposit(_mainAccount, amount)
                {
                    BankManager = _bankManager
                }.Execute();

            try
            {
                new DepositAssumption(_accountBeforeDueDate, deposit)
                {
                    BankManager = _bankManager
                }.Execute();
            }
            catch (InvalidOperationException)
            {
                // ignored
            }

            Assert.AreEqual(result ? deposit : 0m, _accountBeforeDueDate.Balance);
            Assert.AreEqual(result ? amount - deposit : amount, _mainAccount.Balance);
        }

        [Test, TestCaseSource(nameof(DepositTestCases))]
        public void DepositMethodTest(decimal amount, decimal deposit, bool result)
        {
            if (amount > 0)
                new Bank.Transactions.Deposit(_mainAccount, amount)
                {
                    BankManager = _bankManager
                }.Execute();

            try
            {
                new DepositAssumption(_accountBeforeDueDate, deposit)
                {
                    BankManager = _bankManager
                }.Execute();

                if (!result)
                    Assert.Fail();
            }
            catch (InvalidOperationException)
            {
                if (result)
                    Assert.Fail();
            }
        }

        [Test]
        public void TwiceDepositTest()
        {
            new Bank.Transactions.Deposit(_mainAccount, 2m)
            {
                BankManager = _bankManager
            }.Execute();

            var deposits = 0;

            try
            {
                new DepositAssumption(_accountBeforeDueDate, 1m)
                {
                    BankManager = _bankManager
                }.Execute();
                ++ deposits;

                new DepositAssumption(_accountBeforeDueDate, 1m)
                {
                    BankManager = _bankManager
                }.Execute();
                ++deposits;
            }
            catch (InvalidOperationException)
            {
                // ignored
            }

            Assert.AreEqual(1, deposits);
        }

        [Test]
        public void TwiceDepositWithWithdrawTest()
        {
            new Bank.Transactions.Deposit(_mainAccount, 1m)
            {
                BankManager = _bankManager
            }.Execute();

            var deposits = 0;

            try
            {
                new DepositAssumption(_accountBeforeDueDate, 1m)
                {
                    BankManager = _bankManager
                }.Execute();
                ++deposits;

                new DepositWithdraw(_accountBeforeDueDate)
                {
                    BankManager = _bankManager
                }.Execute();

                new DepositAssumption(_accountBeforeDueDate, 1m)
                {
                    BankManager = _bankManager
                }.Execute();
                ++deposits;
            }
            catch (InvalidOperationException)
            {
                // ignored
            }

            Assert.AreEqual(1, deposits);
        }

        [Test, TestCaseSource(nameof(WithdrawTestCases))]
        public void WithdrawAfterDueDateBalanceTest(decimal amount, decimal expected, bool result)
        {
            if (amount > 0)
                new Bank.Transactions.Deposit(_mainAccount, amount)
                {
                    BankManager = _bankManager
                }.Execute();

            try
            {
                new DepositAssumption(_accountAfterDueDate, amount)
                {
                    BankManager = _bankManager
                }.Execute();
                Thread.Sleep(10);
                new DepositWithdraw(_accountAfterDueDate)
                {
                    BankManager = _bankManager
                }.Execute();
            }
            catch (InvalidOperationException)
            {
                // ignored
            }

            Assert.AreEqual(result, _accountAfterDueDate.IsEnded);
            Assert.AreEqual(result ? 0m : expected, _accountAfterDueDate.Balance);
            Assert.AreEqual(result ? expected : 0m, _mainAccount.Balance);
        }

        [Test, TestCaseSource(nameof(WithdrawTestCases))]
        public void WithdrawAfterDueDateTest(decimal amount, decimal expected, bool result)
        {
            if (amount > 0)
                new Bank.Transactions.Deposit(_mainAccount, amount)
                {
                    BankManager = _bankManager
                }.Execute();

            try
            {
                new DepositAssumption(_accountAfterDueDate, amount)
                {
                    BankManager = _bankManager
                }.Execute();
                Thread.Sleep(10);
                new DepositWithdraw(_accountAfterDueDate)
                {
                    BankManager = _bankManager
                }.Execute();

                if (!result)
                    Assert.Fail();
            }
            catch (InvalidOperationException)
            {
                if (result)
                    Assert.Fail();
            }
        }

        [Test, TestCaseSource(nameof(WithdrawTestCases))]
        public void WithdrawBeforeDueDateBalanceTest(decimal amount, decimal expected, bool result)
        {
            if (amount > 0)
                new Bank.Transactions.Deposit(_mainAccount, amount)
                {
                    BankManager = _bankManager
                }.Execute();

            try
            {
                new DepositAssumption(_accountBeforeDueDate, amount)
                {
                    BankManager = _bankManager
                }.Execute();
                Thread.Sleep(10);
                new DepositWithdraw(_accountBeforeDueDate)
                {
                    BankManager = _bankManager
                }.Execute();
            }
            catch (InvalidOperationException)
            {
                // ignored
            }

            Assert.AreEqual(result, _accountBeforeDueDate.IsEnded);
            Assert.AreEqual(result ? 0m : amount, _accountBeforeDueDate.Balance);
            Assert.AreEqual(result ? amount : 0m, _mainAccount.Balance);
        }


        [Test, TestCaseSource(nameof(WithdrawTestCases))]
        public void WithdrawBeforeDueDateTest(decimal amount, decimal expected, bool result)
        {
            if (amount > 0)
                new Bank.Transactions.Deposit(_mainAccount, amount)
                {
                    BankManager = _bankManager
                }.Execute();

            try
            {
                new DepositAssumption(_accountBeforeDueDate, amount)
                {
                    BankManager = _bankManager
                }.Execute();
                Thread.Sleep(10);
                new DepositWithdraw(_accountBeforeDueDate)
                {
                    BankManager = _bankManager
                }.Execute();

                if (!result)
                    Assert.Fail();
            }
            catch (InvalidOperationException)
            {
                if (result)
                    Assert.Fail();
            }
        }
    }
}