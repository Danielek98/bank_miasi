﻿using System;
using Bank;
using Bank.Accounts;
using Bank.Transactions;
using Moq;
using NUnit.Framework;
using Deposit = Bank.Transactions.Deposit;

namespace bankTests.Account
{
    [TestFixture]
    internal class DebitTest
    {
        [SetUp]
        public void SetUp()
        {
            var bankManagerMock = new Mock<BankManager>();
            _account = Standard.CreateStandardAccount(Guid.Empty, null);
            bankManagerMock.Setup(manager => manager.GetAccount<Standard>(It.IsAny<Guid>()))
                .Returns(_account);
            _bankManager = bankManagerMock.Object;
        }

        private Standard _account;
        private BankManager _bankManager;

        private static readonly object[] ChangeDebitLimitTestCases =
        {
            new object[]
            {
                new decimal?[] {100m, 40m, 150m}, new decimal?[] {50m, null, 100m}, new decimal?[] {10m, 10m, null},
                true,
                -130m
            },
            new object[]
            {
                new decimal?[] {100m, 40m, 150m, 200m, 0m}, new decimal?[] {50m, null, 100m, 70m, 50m},
                new decimal?[] {10m, 10m, null, 300m, null}, true, 50m
            },
            new object[]
            {
                new decimal?[] {100m, 40m, 150m, 200m, 0m, null}, new decimal?[] {50m, null, 100m, 70m, 50m, 51m},
                new decimal?[] {10m, 10m, null, 300m, null, null}, false, 50m
            },
            new object[]
            {
                new decimal?[] {100m, 40m, 150m, 200m, 0m, -1m}, new decimal?[] {50m, null, 100m, 70m, 50m, null},
                new decimal?[] {10m, 10m, null, 300m, null, null}, false, 50m
            }
        };

        [Test, TestCaseSource(nameof(ChangeDebitLimitTestCases))]
        public void ChangeDebitLimitBalanceTest(decimal?[] newLimit, decimal?[] toWithdraw, decimal?[] toDeposit,
            bool result, decimal balance)
        {
            try
            {
                for (var i = 0; i < newLimit.Length; ++i)
                {
                    if (newLimit[i].HasValue)
                    {
                        var changeDebit = new ChangeDebit(_account, newLimit[i].Value)
                        {
                            BankManager = _bankManager
                        };
                        changeDebit.Execute();
                        _account = (Standard) changeDebit.ConnectedAccount;
                    }

                    if (toWithdraw[i].HasValue)
                    {
                        new Withdraw(_account, toWithdraw[i].Value)
                        {
                            BankManager = _bankManager
                        }.Execute();
                    }

                    if (toDeposit[i].HasValue)
                    {
                        new Deposit(_account, toDeposit[i].Value)
                        {
                            BankManager = _bankManager
                        }.Execute();
                    }
                }
            }
            catch (Exception)
            {
                // ignored
            }

            Assert.AreEqual(balance, _account.Balance);
        }

        [Test, TestCaseSource(nameof(ChangeDebitLimitTestCases))]
        public void ChangeDebitLimitTest(decimal?[] newLimit, decimal?[] toWithdraw, decimal?[] toDeposit,
            bool result, decimal balance)
        {
            try
            {
                for (var i = 0; i < newLimit.Length; ++i)
                {
                    if (newLimit[i].HasValue)
                    {
                        var changeDebit = new ChangeDebit(_account, newLimit[i].Value)
                        {
                            BankManager = _bankManager
                        };
                        changeDebit.Execute();
                        _account = (Standard) changeDebit.ConnectedAccount;
                    }

                    if (toWithdraw[i].HasValue)
                    {
                        new Withdraw(_account, toWithdraw[i].Value)
                        {
                            BankManager = _bankManager
                        }.Execute();
                    }

                    if (toDeposit[i].HasValue)
                    {
                        new Deposit(_account, toDeposit[i].Value)
                        {
                            BankManager = _bankManager
                        }.Execute();
                    }
                }

                if (!result)
                    Assert.Fail();
            }
            catch (Exception)
            {
                if (result)
                    Assert.Fail();
            }
        }
    }
}