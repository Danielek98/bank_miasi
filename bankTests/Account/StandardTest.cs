﻿using System;
using Bank;
using Bank.Accounts;
using Bank.Interests;
using Bank.Transactions;
using Moq;
using NUnit.Framework;
using Deposit = Bank.Transactions.Deposit;

namespace bankTests.Account
{
    [TestFixture]
    internal class StandardTest
    {
        [SetUp]
        public void SetUp()
        {
            var bankManagerMock = new Mock<BankManager>();
            _account = Standard.CreateStandardAccount(Guid.Empty, null);
            _withDebit = Standard.CreateStandardAccount(Guid.Empty, null);
            bankManagerMock.Setup(manager => manager.GetAccount<Standard>(It.IsAny<Guid>()))
                .Returns<Guid>(guid => guid == _account.AccountNumber ? _account : _withDebit);
            _bankManager = bankManagerMock.Object;

            var changeDebit = new ChangeDebit(_withDebit, DebitAccountLimit)
            {
                BankManager = _bankManager
            };
            changeDebit.Execute();
            _withDebit = (Standard) changeDebit.ConnectedAccount;
        }

        private const decimal DebitAccountLimit = 800;
        private const decimal GreaterDebitAccountLimit = 1000;
        private const decimal SmallerDebitAccountLimit = 600;
        private Standard _account;
        private Standard _withDebit;
        private BankManager _bankManager;

        private static readonly object[] ChangeDebitLimitTestCases =
        {
            new object[] {10m, DebitAccountLimit, GreaterDebitAccountLimit, true},
            new object[] {10m, DebitAccountLimit, SmallerDebitAccountLimit, false},
            new object[] {10m, SmallerDebitAccountLimit, SmallerDebitAccountLimit, true}
        };

        private static readonly object[] WithdrawTestCases =
        {
            new object[] {10m, 3m, 7m, true},
            new object[] {0m, 3m, 0m, false},
            new object[] {5m, 3m, 2m, true},
            new object[] {2m, 3m, 2m, false},
            new object[] {2m, -1m, 2m, false}
        };

        private static readonly object[] WithdrawWithDebitTestCases =
        {
            new object[] {10m, 3m, 7m, true},
            new object[] {0m, 3m, -3m, true},
            new object[] {5m, 3m, 2m, true},
            new object[] {2m, 3m, -1m, true},
            new object[] {2m, -1m, 2m, false},
            new object[] {2m, 3m + DebitAccountLimit, 2m, false},
            new object[] {2m, 2m + DebitAccountLimit, -DebitAccountLimit, true},
            new object[] {2m, 1m + DebitAccountLimit, 1m - DebitAccountLimit, true}
        };

        private static readonly object[] DepositTestCases =
        {
            new object[] {10m, 10m, true},
            new object[] {0m, 0m, false},
            new object[] {-1m, 0m, false}
        };

        private static readonly object[] DepositWithDebitTestCases =
        {
            new object[] {0m, 10m, 10m, true},
            new object[] {5m, 0m, -5m, false},
            new object[] {1m, -1m, -1m, false},
            new object[] {2m, 3m, 1m, true}
        };

        public static readonly object[] ConstructorTestCases =
        {
            new object[] {Guid.Empty, true}
        };

        [Test, TestCaseSource(nameof(ChangeDebitLimitTestCases))]
        public void ChangeDebitLimitTest(decimal depositAmount, decimal withdrawalAmount, decimal newLimit,
            bool expectedChangeResult)
        {
            new Deposit(_withDebit, depositAmount)
            {
                BankManager = _bankManager
            }.Execute();
            new Withdraw(_withDebit, withdrawalAmount)
            {
                BankManager = _bankManager
            }.Execute();
            try
            {
                var changeDebit = new ChangeDebit(_withDebit, newLimit)
                {
                    BankManager = _bankManager
                };
                changeDebit.Execute();
                _withDebit = (Standard) changeDebit.ConnectedAccount;

                if (!expectedChangeResult)
                    Assert.Fail();
            }
            catch (Exception)
            {
                if (expectedChangeResult)
                    Assert.Fail();
            }
        }

        [Test, TestCaseSource(nameof(ConstructorTestCases))]
        public void ConstructorTest(Guid owner, bool result)
        {
            try
            {
                Standard.CreateStandardAccount(owner, ConstantInterest.GetInterest(1.0));

                if (!result)
                    Assert.Fail();
            }
            catch (ArgumentException)
            {
                if (result)
                    Assert.Fail();
            }
        }

        [Test, TestCaseSource(nameof(DepositTestCases))]
        public void DepositBalanceTest(decimal amount, decimal expected, bool result)
        {
            try
            {
                new Deposit(_account, amount)
                {
                    BankManager = _bankManager
                }.Execute();
            }
            catch (InvalidOperationException)
            {
                // ignored
            }

            Assert.AreEqual(expected, _account.Balance);
        }

        [Test, TestCaseSource(nameof(DepositTestCases))]
        public void DepositTest(decimal amount, decimal expected, bool result)
        {
            try
            {
                new Deposit(_account, amount)
                {
                    BankManager = _bankManager
                }.Execute();

                if (!result)
                    Assert.Fail();
            }
            catch (InvalidOperationException)
            {
                if (result)
                    Assert.Fail();
            }
        }

        [Test, TestCaseSource(nameof(DepositWithDebitTestCases))]
        public void DepositWithDebitBalanceTest(decimal withdraw, decimal amount, decimal expected, bool result)
        {
            if (withdraw > 0)
                new Withdraw(_withDebit, withdraw)
                {
                    BankManager = _bankManager
                }.Execute();

            try
            {
                new Deposit(_withDebit, amount)
                {
                    BankManager = _bankManager
                }.Execute();
            }
            catch (InvalidOperationException)
            {
                // ignored
            }

            Assert.AreEqual(expected, _withDebit.Balance);
        }

        [Test, TestCaseSource(nameof(DepositWithDebitTestCases))]
        public void DepositWithDebitTest(decimal withdraw, decimal amount, decimal expected, bool result)
        {
            if (withdraw > 0)
                new Withdraw(_withDebit, withdraw)
                {
                    BankManager = _bankManager
                }.Execute();

            try
            {
                new Deposit(_withDebit, amount)
                {
                    BankManager = _bankManager
                }.Execute();

                if (!result)
                    Assert.Fail();
            }
            catch (InvalidOperationException)
            {
                if (result)
                    Assert.Fail();
            }
        }

        [Test, TestCaseSource(nameof(WithdrawTestCases))]
        public void TransferBalanceExceptionTest(decimal deposit, decimal amount, decimal expected, bool result)
        {
            var bankManagerMock = new Mock<BankManager>();
            var from = Standard.CreateStandardAccount(Guid.Empty, null);
            var to = Standard.CreateStandardAccount(Guid.Empty, null);
            bankManagerMock.Setup(manager => manager.GetAccount<Standard>(It.IsAny<Guid>()))
                .Returns<Guid>(guid => @from.AccountNumber == guid ? @from : null);

            if (deposit > 0)
                new Deposit(from, deposit)
                {
                    BankManager = bankManagerMock.Object
                }.Execute();

            try
            {
                new Transfer(from, amount, to)
                {
                    BankManager = bankManagerMock.Object
                }.Execute();
            }
            catch (InvalidOperationException)
            {
                // ignored
            }

            Assert.AreEqual(expected + (result ? amount : 0m), from.Balance);
        }

        [Test, TestCaseSource(nameof(WithdrawTestCases))]
        public void TransferBalanceTest(decimal deposit, decimal amount, decimal expected, bool result)
        {
            var bankManagerMock = new Mock<BankManager>();
            var from = Standard.CreateStandardAccount(Guid.Empty, null);
            var to = Standard.CreateStandardAccount(Guid.Empty, null);
            bankManagerMock.Setup(manager => manager.GetAccount<Standard>(It.IsAny<Guid>()))
                .Returns<Guid>(guid => @from.AccountNumber == guid ? @from : to);

            if (deposit > 0)
                new Deposit(from, deposit)
                {
                    BankManager = bankManagerMock.Object
                }.Execute();

            Transfer transfer = null;

            try
            {
                transfer = new Transfer(from, amount, to)
                {
                    BankManager = bankManagerMock.Object
                };
                transfer.Execute();
            }
            catch (InvalidOperationException)
            {
                // ignored
            }

            Assert.AreEqual(expected, from.Balance);
            Assert.AreEqual(result ? amount : 0m, transfer?.ConnectedAccount.Balance);
        }

        [Test, TestCaseSource(nameof(WithdrawTestCases))]
        public void TransferExceptionTest(decimal deposit, decimal amount, decimal expected, bool result)
        {
            var bankManagerMock = new Mock<BankManager>();
            var from = Standard.CreateStandardAccount(Guid.Empty, null);
            var to = Standard.CreateStandardAccount(Guid.Empty, null);
            bankManagerMock.Setup(manager => manager.GetAccount<Standard>(It.IsAny<Guid>()))
                .Returns<Guid>(guid => @from.AccountNumber == guid ? @from : null);

            if (deposit > 0)
                new Deposit(from, deposit)
                {
                    BankManager = bankManagerMock.Object
                }.Execute();

            try
            {
                new Transfer(from, amount, to)
                {
                    BankManager = bankManagerMock.Object
                }.Execute();

                Assert.Fail();
            }
            catch (Exception)
            {
                // ignored
            }
        }

        [Test, TestCaseSource(nameof(WithdrawTestCases))]
        public void TransferTest(decimal deposit, decimal amount, decimal expected, bool result)
        {
            var bankManagerMock = new Mock<BankManager>();
            var from = Standard.CreateStandardAccount(Guid.Empty, null);
            var to = Standard.CreateStandardAccount(Guid.Empty, null);
            bankManagerMock.Setup(manager => manager.GetAccount<Standard>(It.IsAny<Guid>()))
                .Returns<Guid>(guid => @from.AccountNumber == guid ? @from : to);

            if (deposit > 0)
                new Deposit(from, deposit)
                {
                    BankManager = bankManagerMock.Object
                }.Execute();

            try
            {
                new Transfer(from, amount, to)
                {
                    BankManager = bankManagerMock.Object
                }.Execute();

                if (!result)
                    Assert.Fail();
            }
            catch (Exception)
            {
                if (result)
                    Assert.Fail();
            }
        }

        [Test, TestCaseSource(nameof(WithdrawTestCases))]
        public void WithdrawBalanceTest(decimal deposit, decimal amount, decimal expected, bool result)
        {
            if (deposit > 0)
                new Deposit(_account, deposit)
                {
                    BankManager = _bankManager
                }.Execute();

            try
            {
                new Withdraw(_account, amount)
                {
                    BankManager = _bankManager
                }.Execute();
            }
            catch (InvalidOperationException)
            {
                // ignored
            }

            Assert.AreEqual(expected, _account.Balance);
        }

        [Test, TestCaseSource(nameof(WithdrawTestCases))]
        public void WithdrawTest(decimal deposit, decimal amount, decimal expected, bool result)
        {
            if (deposit > 0)
                new Deposit(_account, deposit)
                {
                    BankManager = _bankManager
                }.Execute();

            try
            {
                new Withdraw(_account, amount)
                {
                    BankManager = _bankManager
                }.Execute();

                if (!result)
                    Assert.Fail();
            }
            catch (InvalidOperationException)
            {
                if (result)
                    Assert.Fail();
            }
        }

        [Test, TestCaseSource(nameof(WithdrawWithDebitTestCases))]
        public void WithdrawWithDebitBalanceTest(decimal deposit, decimal amount, decimal expected, bool result)
        {
            if (deposit > 0)
                new Deposit(_withDebit, deposit)
                {
                    BankManager = _bankManager
                }.Execute();

            try
            {
                new Withdraw(_withDebit, amount)
                {
                    BankManager = _bankManager
                }.Execute();
            }
            catch (InvalidOperationException)
            {
                // ignored
            }

            Assert.AreEqual(expected, _withDebit.Balance);
        }

        [Test, TestCaseSource(nameof(WithdrawWithDebitTestCases))]
        public void WithdrawWithDebitTest(decimal deposit, decimal amount, decimal expected, bool result)
        {
            if (deposit > 0)
                new Deposit(_withDebit, deposit)
                {
                    BankManager = _bankManager
                }.Execute();

            try
            {
                new Withdraw(_withDebit, amount)
                {
                    BankManager = _bankManager
                }.Execute();

                if (!result)
                    Assert.Fail();
            }
            catch (InvalidOperationException)
            {
                if (result)
                    Assert.Fail();
            }
        }
    }
}