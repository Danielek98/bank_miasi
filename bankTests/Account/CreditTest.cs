﻿using System;
using Bank;
using Bank.Accounts;
using Bank.Interests;
using Bank.Transactions;
using Moq;
using NUnit.Framework;
using Deposit = Bank.Transactions.Deposit;

namespace bankTests.Account
{
    [TestFixture]
    internal class CreditTest
    {
        [SetUp]
        public void SetUp()
        {
            var bankManagerMock = new Mock<BankManager>();
            _mainAccount = Standard.CreateStandardAccount(Guid.Empty, null);
            _interest = ConstantInterest.GetInterest(InterestFactor);
            _account = Credit.CreateCredit(_mainAccount, _interest, CreditLimit);
            bankManagerMock.Setup(manager => manager.GetAccount<Credit>(It.IsAny<Guid>())).Returns(_account);
            bankManagerMock.Setup(manager => manager.GetAccount<Standard>(It.IsAny<Guid>())).Returns(_mainAccount);
            _bankManager = bankManagerMock.Object;
        }

        private const decimal CreditLimit = 100m;
        private const double InterestFactor = 1.5;
        private Credit _account;
        private Standard _mainAccount;
        private InterestBase _interest;
        private BankManager _bankManager;

        private static readonly object[] WithdrawTestCases =
        {
            new object[] {3m, true},
            new object[] {CreditLimit, true},
            new object[] {-5m, false},
            new object[] {0m, false},
            new object[] {CreditLimit + 1m, false}
        };

        private static readonly object[] DepositTestCases =
        {
            new object[] {10m, 15m, true},
            new object[] {CreditLimit, CreditLimit*1.5m, true},
            new object[] {10m, 14m, false},
            new object[] {10m, 16m, true},
            new object[] {0m, 0m, false}
        };

        public static readonly object[] ConstructorTestCases =
        {
            new object[] {true, true, 1m, true},
            new object[] {false, true, 1m, false},
            new object[] {true, false, 1m, false},
            new object[] {true, true, 0m, false}
        };

        [Test, TestCaseSource(nameof(ConstructorTestCases))]
        public void ConstructorTest(bool account, bool interest, decimal limit, bool result)
        {
            try
            {
                Credit.CreateCredit(account ? _mainAccount : null, interest ? _interest : null, limit);

                if (!result)
                    Assert.Fail();
            }
            catch (ArgumentOutOfRangeException)
            {
                if (result)
                    Assert.Fail();
            }
            catch (ArgumentNullException)
            {
                if (result)
                    Assert.Fail();
            }
        }

        [Test, TestCaseSource(nameof(DepositTestCases))]
        public void DepositBalanceTest(decimal amount, decimal expected, bool result)
        {
            if (expected - amount > 0)
                new Deposit(_mainAccount, expected - amount)
                {
                    BankManager = _bankManager
                }.Execute();

            if (amount > 0)
                new LoanTakeOut(_account, amount)
                {
                    BankManager = _bankManager
                }.Execute();

            var toPay = _account.Balance;

            try
            {
                new LoanRepayment(_account)
                {
                    BankManager = _bankManager
                }.Execute();
            }
            catch (InvalidOperationException)
            {
                // ignored
            }

            Assert.AreEqual(result, _account.IsPaid);
            Assert.AreEqual(result ? 0m : toPay, _account.Balance);
            Assert.AreEqual(result ? expected + toPay : expected, _mainAccount.Balance);
        }

        [Test, TestCaseSource(nameof(DepositTestCases))]
        public void DepositTest(decimal amount, decimal expected, bool result)
        {
            if (expected - amount > 0)
                new Deposit(_mainAccount, expected - amount)
                {
                    BankManager = _bankManager
                }.Execute();

            if (amount > 0)
                new LoanTakeOut(_account, amount)
                {
                    BankManager = _bankManager
                }.Execute();

            try
            {
                new LoanRepayment(_account)
                {
                    BankManager = _bankManager
                }.Execute();

                if (!result)
                    Assert.Fail();
            }
            catch (InvalidOperationException)
            {
                if (result)
                    Assert.Fail();
            }
        }

        [Test]
        public void TwiceWithdrawTest()
        {
            var withdraws = 0;
            try
            {
                new LoanTakeOut(_account, 1m)
                {
                    BankManager = _bankManager
                }.Execute();
                ++ withdraws;
                new LoanTakeOut(_account, 1m)
                {
                    BankManager = _bankManager
                }.Execute();
                ++withdraws;
            }
            catch (InvalidOperationException)
            {
                // ignored
            }

            Assert.AreEqual(1, withdraws);
        }

        [Test]
        public void TwiceWithdrawWithDepositTest()
        {
            var withdraws = 0;
            try
            {
                new LoanTakeOut(_account, 1m)
                {
                    BankManager = _bankManager
                }.Execute();
                ++withdraws;

                new Deposit(_mainAccount, -_account.Balance)
                {
                    BankManager = _bankManager
                }.Execute();
                new LoanRepayment(_account)
                {
                    BankManager = _bankManager
                }.Execute();

                new LoanTakeOut(_account, 1m)
                {
                    BankManager = _bankManager
                }.Execute();
                ++withdraws;
            }
            catch (InvalidOperationException)
            {
                // ignored
            }

            Assert.AreEqual(1, withdraws);
        }

        [Test, TestCaseSource(nameof(WithdrawTestCases))]
        public void WithdrawBalanceTest(decimal amount, bool result)
        {
            try
            {
                new LoanTakeOut(_account, amount)
                {
                    BankManager = _bankManager
                }.Execute();
            }
            catch (InvalidOperationException)
            {
                // ignored
            }

            var accountBase = new Mock<AccountBase>(Guid.Empty, _interest);
            accountBase.SetupGet(account => account.Balance).Returns(-amount);
            accountBase.SetupGet(account => account.Interest).Returns(_interest);
            decimal toPay = -1;
            accountBase.SetupSet(account => account.Balance = It.IsAny<decimal>())
                .Callback<decimal>(value => toPay = value);
            new CalculateInterest<AccountBase>(accountBase.Object).Execute();

            Assert.AreEqual(result, _account.IsTaken);
            Assert.AreEqual(result ? toPay : 0m, _account.Balance);
            Assert.AreEqual(result ? amount : 0m, _mainAccount.Balance);
        }

        [Test, TestCaseSource(nameof(WithdrawTestCases))]
        public void WithdrawTest(decimal amount, bool result)
        {
            try
            {
                new LoanTakeOut(_account, amount)
                {
                    BankManager = _bankManager
                }.Execute();

                if (!result)
                    Assert.Fail();
            }
            catch (InvalidOperationException)
            {
                if (result)
                    Assert.Fail();
            }
        }
    }
}