﻿using System;
using System.Linq;
using Bank;
using Bank.Accounts;
using Moq;
using NUnit.Framework;

namespace bankTests
{
    [TestFixture]
    internal class BankManagerTest
    {
        [SetUp]
        public void SetUp()
        {
            _bank = new Bank.Bank("TestBank");
            _clientMock.Setup(client => client.ClientId).Returns(Guid.Empty);
        }

        private Bank.Bank _bank;
        private readonly Mock<Client> _clientMock = new Mock<Client>("John", "Doe");

        [Test]
        public void GetAccountTest()
        {
            _bank.RegisterNewClient(_clientMock.Object);
            var accountNumber = _bank.CreateNewStandardAccount(_clientMock.Object.ClientId);

            Assert.IsNotNull(new BankManager().GetAccount<Standard>(accountNumber));
        }

        [Test]
        public void GetBankTest()
        {
            _bank.RegisterNewClient(_clientMock.Object);
            var accountNumber = _bank.CreateNewStandardAccount(_clientMock.Object.ClientId);
            Assert.AreSame(_bank, new BankManager().GetBank(accountNumber));
        }

        [Test]
        public void GetProductsTest()
        {
            _bank.RegisterNewClient(_clientMock.Object);
            var accountNumber = _bank.CreateNewStandardAccount(_clientMock.Object.ClientId);

            Assert.Contains(accountNumber,
                new BankManager().GetProducts(_clientMock.Object).Select(account => account.AccountNumber).ToList());
        }
    }
}