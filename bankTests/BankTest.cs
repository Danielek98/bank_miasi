﻿using System;
using System.Linq;
using Bank;
using Bank.Accounts;
using Moq;
using NUnit.Framework;

namespace bankTests
{
    [TestFixture]
    internal class BankTest
    {
        [SetUp]
        public void SetUp()
        {
            _bank = new Bank.Bank("TestBank");
            _clientMock.Setup(client => client.ClientId).Returns(Guid.Empty);
        }

        private Bank.Bank _bank;
        private readonly Mock<Client> _clientMock = new Mock<Client>("John", "Doe");

        [Test]
        public void CreateNewAccountAsNotRegisteredTest()
        {
            Assert.Throws<Exception>(() => _bank.CreateNewStandardAccount(_clientMock.Object.ClientId));
        }

        [Test]
        public void CreateNewCreditTest()
        {
            _bank.RegisterNewClient(_clientMock.Object);
            var standardAccountNumber = _bank.CreateNewStandardAccount(_clientMock.Object.ClientId);
            var accountNumber = _bank.CreateNewCredit(standardAccountNumber, 2000m);
            var accounts = _bank.GetClientAccounts(_clientMock.Object);

            Assert.Contains(accountNumber, accounts.Select(account => account.AccountNumber).ToList());
        }

        [Test]
        public void CreateNewDebitTest()
        {
            _bank.RegisterNewClient(_clientMock.Object);
            var standardAccountNumber = _bank.CreateNewStandardAccount(_clientMock.Object.ClientId);
            var dueTime = TimeSpan.FromDays(1);
            var accountNumber = _bank.CreateNewDeposit(standardAccountNumber, dueTime);
            var accounts = _bank.GetClientAccounts(_clientMock.Object);

            Assert.Contains(accountNumber, accounts.Select(account => account.AccountNumber).ToList());
        }

        [Test]
        public void CreateNewStandardAccountTest()
        {
            _bank.RegisterNewClient(_clientMock.Object);
            var accountNumber = _bank.CreateNewStandardAccount(_clientMock.Object.ClientId);
            var accounts = _bank.GetClientAccounts(_clientMock.Object);

            Assert.Contains(accountNumber, accounts.Select(account => account.AccountNumber).ToList());
        }

        [Test]
        public void GetAllAccountsTest()
        {
            _bank.RegisterNewClient(_clientMock.Object);
            _bank.CreateNewStandardAccount(_clientMock.Object.ClientId);
            var accounts = _bank.GetClientAccounts(_clientMock.Object);

            Assert.Contains(accounts.Last(), new BankManager().GetAllAccounts().ToList());
        }

        [Test]
        public void RegisterNewClientTest()
        {
            _bank.RegisterNewClient(_clientMock.Object);
            Assert.IsTrue(_bank.Clients.ContainsKey(_clientMock.Object));
        }

        [Test]
        public void ReplaceStandardAccountExceptionTest()
        {
            _bank.RegisterNewClient(_clientMock.Object);
            var account = Standard.CreateStandardAccount(_clientMock.Object.ClientId, null);
            var newAccount = Standard.CreateStandardAccount(_clientMock.Object.ClientId, null);
            try
            {
                _bank.ReplaceAccount(account, newAccount);
                Assert.Fail();
            }
            catch (Exception)
            {
                // ignored
            }
        }

        [Test]
        public void ReplaceStandardAccountTest()
        {
            _bank.RegisterNewClient(_clientMock.Object);
            var accountNumber = _bank.CreateNewStandardAccount(_clientMock.Object.ClientId);
            var newAccount = Standard.CreateStandardAccount(_clientMock.Object.ClientId, null);
            _bank.ReplaceAccount(new BankManager().GetAccount<Standard>(accountNumber), newAccount);

            Assert.AreEqual(new[] {newAccount}, _bank.GetClientAccounts(_clientMock.Object));
        }
    }
}