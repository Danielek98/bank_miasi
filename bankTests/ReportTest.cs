﻿using System;
using System.Collections.Generic;
using Bank;
using Bank.Accounts;
using Bank.Interests;
using Bank.Transactions;
using Moq;
using NUnit.Framework;
using Deposit = Bank.Transactions.Deposit;

namespace bankTests
{
    internal class ReportTest
    {
        private static readonly Mock<Standard>[] StandardAccountMocks =
        {
            new Mock<Standard>(Guid.NewGuid(), new Mock<InterestBase>().Object),
            new Mock<Standard>(Guid.NewGuid(), new Mock<InterestBase>().Object),
            new Mock<Standard>(Guid.NewGuid(), new Mock<InterestBase>().Object),
            new Mock<Standard>(Guid.NewGuid(), new Mock<InterestBase>().Object),
            new Mock<Standard>(Guid.NewGuid(), new Mock<InterestBase>().Object),
            new Mock<Standard>(Guid.NewGuid(), new Mock<InterestBase>().Object)
        };

        private static readonly Mock<Client>[] ClientMocks =
        {
            new Mock<Client>("John", "Doe"),
            new Mock<Client>("John", "Smith")
        };

        private static readonly Dictionary<Client, IEnumerable<AccountBase>> Accounts =
            new Dictionary<Client, IEnumerable<AccountBase>>
            {
                {
                    ClientMocks[0].Object, new AccountBase[]
                    {
                        StandardAccountMocks[0].Object,
                        StandardAccountMocks[1].Object,
                        StandardAccountMocks[2].Object,
                        StandardAccountMocks[3].Object
                    }
                },
                {
                    ClientMocks[1].Object, new AccountBase[]
                    {
                        StandardAccountMocks[4].Object,
                        StandardAccountMocks[5].Object
                    }
                }
            };

        private static readonly Mock<History> HistoryMock = new Mock<History>();

        [SetUp]
        public void SetUp()
        {
            HistoryMock.Setup(history => history.GetHistory())
                .Returns(new List<Command>
                {
                    new Deposit(StandardAccountMocks[0].Object, 10m),
                    new Withdraw(StandardAccountMocks[0].Object, 5m)
                });

            foreach (var accountMock in StandardAccountMocks)
            {
                accountMock.SetupGet(account => account.Balance).Returns(accountMock.Object.DebitLimit);
            }
        }

        [Test]
        public void GenerateHistoryTest()
        {
            var result = HistoryMock.Object.GetHistory().GenerateHistoryReport();
            Assert.AreEqual($"{typeof (Deposit).Name}: {StandardAccountMocks[0].Object.AccountNumber}\t{10m}\t\n"
                            + $"{typeof (Withdraw).Name}: {StandardAccountMocks[0].Object.AccountNumber}\t{5m}\t",
                result);
        }

        [Test]
        public void CalculateTotalAssetsPerClientTest()
        {
            var result = Accounts.CalculateTotalAssetsPerClient();
            Assert.AreEqual(0m, result[ClientMocks[0].Object]);
            Assert.AreEqual(0m, result[ClientMocks[1].Object]);
        }

        [Test]
        public void GetProductsPerClientTest()
        {
            var result = Accounts.GetProductsPerClient();
            Assert.AreEqual(4, result[ClientMocks[0].Object].Count);
            Assert.AreEqual(2, result[ClientMocks[1].Object].Count);
        }

        [Test]
        public void CalculateTotalAssetsTest()
        {
            var result = Accounts.CalculateTotalAssets();
            Assert.AreEqual(0m, result);
        }

        [Test]
        public void GetProductsTest()
        {
            var result = Accounts.GetProducts();
            Assert.AreEqual(6, result.Count);
        }
    }
}