﻿using System;
using Bank.Accounts;
using Bank.Interests;
using Bank.Transactions;
using Moq;
using NUnit.Framework;

namespace bankTests.Interest
{
    [TestFixture]
    internal class ConstantInterestTest
    {
        private static readonly object[] CalculateTestCases =
        {
            new object[] {1500m, 1.5, 1000m},
            new object[] {1000m, 1.0, 1000m},
            new object[] {-1500m, 1.5, -1000m},
            new object[] {1333.3m, 1.3333, 1000m},
            new object[] {1333.33m, 1.333333, 1000m}
        };

        [Test, TestCaseSource(nameof(CalculateTestCases))]
        public void CalculateTest(decimal expected, double factor, decimal amount)
        {
            var interest = ConstantInterest.GetInterest(factor);
            var accountBaseMock = new Mock<AccountBase>(Guid.Empty, interest);
            accountBaseMock.SetupGet(account => account.Interest).Returns(interest);
            accountBaseMock.SetupGet(account => account.Balance).Returns(amount);
            decimal? result = null;
            accountBaseMock.SetupSet(account => account.Balance = It.IsAny<decimal>())
                .Callback<decimal>(value => result = value);

            new CalculateInterest<AccountBase>(accountBaseMock.Object).Execute();
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void InvalidConstructorTest()
        {
            try
            {
                ConstantInterest.GetInterest(0.5);
                Assert.Fail();
            }
            catch (ArgumentException)
            {
                // ignored
            }
        }
    }
}