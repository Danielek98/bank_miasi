﻿using System;
using Bank;
using Bank.Accounts;
using Bank.Transactions;
using Moq;
using NUnit.Framework;
using Deposit = Bank.Transactions.Deposit;

namespace bankTests.Transactions
{
    [TestFixture]
    internal class HistoryTest
    {
        [SetUp]
        public void SetUp()
        {
            _history = new History();
            _account = Standard.CreateStandardAccount(Guid.Empty, null);
            var bankManagerMock = new Mock<BankManager>();
            bankManagerMock.Setup(manager => manager.GetAccount<Standard>(It.IsAny<Guid>())).Returns(_account);
            _bankManager = bankManagerMock.Object;
        }

        private History _history;
        private Standard _account;
        private BankManager _bankManager;

        [Test]
        public void RegisterTest()
        {
            _history.Register(_account);
            new Deposit(_account, 10m)
            {
                BankManager = _bankManager
            }.Execute();
            Assert.IsNotEmpty(_history.GetHistory());
        }

        [Test]
        public void UnregisterTest()
        {
            _history.Register(_account);
            _history.Unregister(_account);
            new Deposit(_account, 10m)
            {
                BankManager = _bankManager
            }.Execute();
            Assert.IsEmpty(_history.GetHistory(_account));
        }
    }
}